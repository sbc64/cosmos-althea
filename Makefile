all: build-docker

gaiadImage='tendermint/gaiad:latest'
project='gaia'

build-docker: 
	docker build -t tendermint/gaiad:latest .

run:
	docker run tendermint/gaiad:latest

deploy: build-docker
	GAIAD_IMAGE=$(gaiadImage) \
	docker stack deploy -c ops/gaia-compose.yml \
	$(project)

stop:
	docker stack rm $(project)
	while [ -n "`docker network ls --quiet --filter label=com.docker.stack.namespace=$(project)`" ]; do echo -n '.' && sleep 1; done
	@echo  "MAKE: Done with $@"
	@echo
