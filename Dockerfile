# Simple usage with a mounted data directory:
# > docker build -t gaia .
# > docker run -it -p 46657:46657 -p 46656:46656 -v ~/.gaiad:/root/.gaiad -v ~/.gaiacli:/root/.gaiacli gaia gaiad init
# > docker run -it -p 46657:46657 -p 46656:46656 -v ~/.gaiad:/root/.gaiad -v ~/.gaiacli:/root/.gaiacli gaia gaiad start
# FROM golang:alpine AS build-env
# WORKDIR /gaia

# Set up dependencies
# RUN apk add --no-cache curl make git libc-dev bash gcc linux-headers eudev-dev python

# RUN git clone https://github.com/cosmos/gaia /gaia
# RUN git checkout v2.0.3
# RUN make build

# Final image
# FROM alpine:edge 
# using this debian image because the binaries 
# in ./bin were built for ubunt
FROM debian:stable-slim

# Install ca-certificates
#RUN apk add --no-cache --update ca-certificates

# Copy over binaries from the build-env
# COPY --from=build-env /gaia/build/gaiad /usr/bin/gaiad
# COPY --from=build-env /gaia/build/gaiacli /usr/bin/gaiacli
ADD ./config /root/.gaiad/config
ADD ./althea-zone/genesis.json /root/.gaiad/config/genesis.json
RUN ls /root/.gaiad/config

# Run gaiad by default, omit entrypoint to ease using container with gaiacli
COPY ./bin/gaiad /usr/bin/gaiad
COPY ./bin/gaiacli /usr/bin/gaiacli
CMD ["/usr/bin/gaiad", "start"]
