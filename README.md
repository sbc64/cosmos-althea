# Instructions for starting a testnet:


## Setup
- Install go
- Install [gaia](https://github.com/sebohe/gaia/tree/working) at the `working` branch
- [Generate cosmos keys](#generate-cosmos-keys)
- [Generate gentx](#generate-gentx)

### Generate cosmos keys
```
gaiacli keys add <name-of-key>

```

### Generate gentx
```
gaiad init <name of computer>
gaiad add-genesis-account <name-of-key> <quantity of coins><coin symbol> #example: 1000dollar,4000altg

gaiad gentx \
  --amount 10000dollar \
  --commission-rate 0.001 \
  --commission-max-rate 0.01 \
  --commission-max-change-rate 0.2 \
  --name althea-cosmos
```

### Genesis parameters

To find out more of these parameters do the follwing: `grep -rf <cosmos-sdk-repo> <parameter>`

`max_deposit_period`: The maximum period (in **nanoseconds**) after which it is not possible to deposit on the proposal anymore. 864000000000000 nano seconds, or 10 days

`voting_params.voting_period`: "864000000000000"

`max_evidence_age`: Maximum age of the evidence in **nanoseconds**.

`min_signed_per_window`: Minimum percentage of `precommits`that must be present in the `block window` for the validator to be considered online.

`signed_blocks_window`: Moving window of blocks to figure out offline validators.

`unbonding_time`: Time in **nanosecond** it takes for tokens to complete unbonding.

`max_bytes`: Maximum number of bytes per block.


Showing Ute something about Git
