#!/usr/bin/env bash

ip=$(curl -s ipecho.net/plain)
validator=$(gaiad tendermint show-validator)
key=$(gaiacli keys list | cut -f1 | sed -n '2p')

# backup to tmp
cp -r ./gentx /tmp/gentx
# we need to remove it because gaiad complains
rm ~/.gaiad/config/gentx

gaiad gentx \
  --amount 100000000000stake \
  --commission-rate 0.1  \
  --commission-max-rate 0.2 \
  --commission-max-change-rate 0.01 \
  --pubkey $validator \
  --name $key \
  --ip $ip

# move newly created gentx to repo
cp ~/.gaiad/config/gentx/* ./gentx/
# remove directory
rm -r ~/.gaiad/config/gentx
# link repo gentx to default gentx
ln -s `readlink -f ./gentx` ~/.gaiad/config/gentx

for filename in ./gentx/*; do
  ./ops/sort-json.sh $filename
done


