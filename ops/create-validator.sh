#!/usr/bin/env bash

name="ocean"
amount="100001000ualtg "

gaiacli tx staking create-validator \
  --amount=$amount\
  --commission-rate="0.10" \
  --commission-max-rate="0.20" \
  --commission-max-change-rate="0.01" \
  --min-self-delegation="1" \
  --pubkey=$(gaiad tendermint show-validator) \
  --moniker=$(gaiacli status | jq '.node_info.moniker')
  --chain-id=$(gaiacli status | jq '.node_info.network')
  --from=$($ gaiacli keys show $name -a)
