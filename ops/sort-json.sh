#!/usr/bin/env bash

cat $1 | jq -S '.' > /tmp/sorted.json
mv /tmp/sorted.json $1
